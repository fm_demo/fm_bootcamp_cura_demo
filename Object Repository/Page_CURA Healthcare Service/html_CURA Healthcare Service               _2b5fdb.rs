<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>html_CURA Healthcare Service               _2b5fdb</name>
   <tag></tag>
   <elementGuidId>e03dcc51-14cf-4112-80a2-3454f17d0de7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*/text()[normalize-space(.)='']/parent::*</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>html</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>html</value>
      <webElementGuid>c70e8289-3ae2-4ee2-aeeb-61eee7ce27a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>lang</name>
      <type>Main</type>
      <value>en</value>
      <webElementGuid>5307d47a-2452-4260-bd8c-acb81ed69a0f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    
    
    
    

    CURA Healthcare Service

    
    
    

    
    

    
    
    

    
    
    

#katalon{font-family:monospace;font-size:13px;background-color:rgba(0,0,0,.7);position:fixed;top:0;left:0;right:0;display:block;z-index:999999999;line-height: normal} #katalon div{padding:0;margin:0;color:#fff;} #katalon kbd{display:inline-block;padding:3px 5px;font:13px Consolas,&quot;Liberation Mono&quot;,Menlo,Courier,monospace;line-height:10px;color:#555;vertical-align:middle;background-color:#fcfcfc;border:1px solid #ccc;border-bottom-color:#bbb;border-radius:3px;box-shadow:inset 0 -1px 0 #bbb;font-weight: bold} div#katalon-spy_elementInfoDiv {color: lightblue; padding: 0px 5px 5px} div#katalon-spy_instructionDiv {padding: 5px 5px 2.5px}





    
        
        
            CURA Healthcare
        
        
            Home
        
                
            Login
        
            




    
        CURA Healthcare Service
        We Care About Your Health
        
        Make Appointment
    



    
        
            
                Login
                Please login to make appointment.
                                    Login failed! Please ensure the username and password are valid.
                            
            
                
                    
                        
                            Demo account
                            
                                
                                  
                                  
                                
                            
                        
                        
                            
                                
                                  
                                  
                                
                            
                        
                    
                    
                        Username
                        
                            
                        
                    
                    
                        Password
                        
                            
                        
                    
                    
                        
                            Login
                        
                    
                
            
        
    




    
        
            
                CURA Healthcare Service
                
                Atlanta 550 Pharr Road NE Suite 525Atlanta, GA 30305
                
                     (678) 813-1KMS
                     info@katalon.com
                    
                
                
                
                    
                        
                    
                    
                        
                    
                    
                        
                    
                
                
                Copyright © CURA Healthcare Service 2023
            
        
    
    










/html[1]</value>
      <webElementGuid>eabb0891-83e1-4fc2-a297-dec4b3769503</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]</value>
      <webElementGuid>9c0a98b2-f700-4de2-b64a-cd6d10489eb0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='']/parent::*</value>
      <webElementGuid>29097be4-23d7-452e-ba35-862f15560789</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//html</value>
      <webElementGuid>1fa36ffd-24b5-4c75-82ca-d994662eb3b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//html[(text() = '
    
    
    
    
    

    CURA Healthcare Service

    
    
    

    
    

    
    
    

    
    
    

#katalon{font-family:monospace;font-size:13px;background-color:rgba(0,0,0,.7);position:fixed;top:0;left:0;right:0;display:block;z-index:999999999;line-height: normal} #katalon div{padding:0;margin:0;color:#fff;} #katalon kbd{display:inline-block;padding:3px 5px;font:13px Consolas,&quot;Liberation Mono&quot;,Menlo,Courier,monospace;line-height:10px;color:#555;vertical-align:middle;background-color:#fcfcfc;border:1px solid #ccc;border-bottom-color:#bbb;border-radius:3px;box-shadow:inset 0 -1px 0 #bbb;font-weight: bold} div#katalon-spy_elementInfoDiv {color: lightblue; padding: 0px 5px 5px} div#katalon-spy_instructionDiv {padding: 5px 5px 2.5px}





    
        
        
            CURA Healthcare
        
        
            Home
        
                
            Login
        
            




    
        CURA Healthcare Service
        We Care About Your Health
        
        Make Appointment
    



    
        
            
                Login
                Please login to make appointment.
                                    Login failed! Please ensure the username and password are valid.
                            
            
                
                    
                        
                            Demo account
                            
                                
                                  
                                  
                                
                            
                        
                        
                            
                                
                                  
                                  
                                
                            
                        
                    
                    
                        Username
                        
                            
                        
                    
                    
                        Password
                        
                            
                        
                    
                    
                        
                            Login
                        
                    
                
            
        
    




    
        
            
                CURA Healthcare Service
                
                Atlanta 550 Pharr Road NE Suite 525Atlanta, GA 30305
                
                     (678) 813-1KMS
                     info@katalon.com
                    
                
                
                
                    
                        
                    
                    
                        
                    
                    
                        
                    
                
                
                Copyright © CURA Healthcare Service 2023
            
        
    
    










/html[1]' or . = '
    
    
    
    
    

    CURA Healthcare Service

    
    
    

    
    

    
    
    

    
    
    

#katalon{font-family:monospace;font-size:13px;background-color:rgba(0,0,0,.7);position:fixed;top:0;left:0;right:0;display:block;z-index:999999999;line-height: normal} #katalon div{padding:0;margin:0;color:#fff;} #katalon kbd{display:inline-block;padding:3px 5px;font:13px Consolas,&quot;Liberation Mono&quot;,Menlo,Courier,monospace;line-height:10px;color:#555;vertical-align:middle;background-color:#fcfcfc;border:1px solid #ccc;border-bottom-color:#bbb;border-radius:3px;box-shadow:inset 0 -1px 0 #bbb;font-weight: bold} div#katalon-spy_elementInfoDiv {color: lightblue; padding: 0px 5px 5px} div#katalon-spy_instructionDiv {padding: 5px 5px 2.5px}





    
        
        
            CURA Healthcare
        
        
            Home
        
                
            Login
        
            




    
        CURA Healthcare Service
        We Care About Your Health
        
        Make Appointment
    



    
        
            
                Login
                Please login to make appointment.
                                    Login failed! Please ensure the username and password are valid.
                            
            
                
                    
                        
                            Demo account
                            
                                
                                  
                                  
                                
                            
                        
                        
                            
                                
                                  
                                  
                                
                            
                        
                    
                    
                        Username
                        
                            
                        
                    
                    
                        Password
                        
                            
                        
                    
                    
                        
                            Login
                        
                    
                
            
        
    




    
        
            
                CURA Healthcare Service
                
                Atlanta 550 Pharr Road NE Suite 525Atlanta, GA 30305
                
                     (678) 813-1KMS
                     info@katalon.com
                    
                
                
                
                    
                        
                    
                    
                        
                    
                    
                        
                    
                
                
                Copyright © CURA Healthcare Service 2023
            
        
    
    










/html[1]')]</value>
      <webElementGuid>63f72c7f-1645-41bd-b9e1-479ed084e35d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
